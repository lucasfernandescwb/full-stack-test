# Full Stack Test Application

Este é um projeto de teste full stack (Movies). Siga os passos abaixo para configurar e executar a aplicação localmente.

## Clonar Aplicação

Clone o repositório da aplicação:

```bash
git clone https://gitlab.com/lucasfernandescwb/full-stack-test
```

## Entrar na Pasta Full Stack Test e Trocar para a Branch Develop

```bash
cd full-stack-test
git checkout develop
```

## Configurar o Backend

1. Entre no diretório do backend:

```bash
cd backend
```

2. Copie o arquivo `.env.example` e renomeie para `.env`. Configure os campos `DB_USERNAME` como `xrosoff` e `DB_PASSWORD` como `pocky123`.

3. Instale as dependências com o Composer:

```bash
composer install
```

4. Execute os comandos Docker:

```bash
docker-compose up -d
docker-compose exec app bash
```

5. Gere a chave da aplicação (caso necessário):

```bash
php artisan key:generate
```

6. Rode as migrations:

```bash
php artisan migrate:fresh
```

7. Ative o link simbólico:

```bash
php artisan storage:link
```

## Configurar o Frontend

1. Volte ao diretório raiz do projeto e entre no diretório frontend:

```bash
cd ..
cd frontend
```

2. Instale as dependências utilizando npm ou yarn:

```bash
npm install
# ou
yarn
```

3. Execute o servidor de desenvolvimento:

```bash
npm run dev
```

## Acesso à Aplicação

Após seguir os passos acima, a aplicação frontend estará disponível no navegador. Acesse o seguinte endereço:

```
http://localhost:5173
```

## Autor

Lucas Fernandes Lima
